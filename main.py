from typing import Optional

from fastapi import FastAPI

import requests

app = FastAPI()

import json
def load_params_from_json(json_path):
    with open(json_path) as f:
        return json.load(f)


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: Optional[str] = None):
    return {"item_id": item_id, "q": q}


requete = requests.get("https://world.openfoodfacts.org/api/v0/product/3256540001305.json")
#print(requete.status_code)
#print(requete.json())

def print_item(item_id):
    requete = requests.get("https://world.openfoodfacts.org/api/v0/product/" + item_id + ".json")
    return(requete.json())


item1 = print_item("3256540001305")
#print(item1)

@app.get("/isVegan/{item_id}")
def isVegan2(requestAsJson):
    ingredients = requestAsJson["product"]["ingredients"]
    print(ingredients)
    return True


def isVegan(requestAsJson):
    ingredients = requestAsJson["product"]["ingredients"]
    for ingredient in ingredients:
        if 'vegan' in ingredient:
          if ingredient["vegan"]=='no' or ingredient["vegan"]=='maybe':
            return False
    return True



isVegan(item1)
